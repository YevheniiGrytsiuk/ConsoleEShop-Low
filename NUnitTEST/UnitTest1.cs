using NUnit.Framework;
using System;
using TEST;
using TEST.Entities;
using TEST.Interfaces;
using TEST.RoledUsers;

namespace NUnitTEST
{
    [TestFixture]
    public class Tests
    {
        public enum Roles
        {
            Guest,
            Registered,
            Admin
        }
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void GuestInstanceShoudHaveGuestRole()
        {
            //arrange
            Guest guest = new Guest();
            var role = UserManager.GetCurrentUserRole();

            //act
            var expectedRole = UserManager.Roles.Guest;

            //assert
            Assert.AreEqual(expectedRole, role, message: "Expected role guest.");
        }
        [TestCase("viktor", "yolo", "viktor@mail.com")]
        [TestCase("testName", "pwd", "test@mail.com")]
        public void RegisteredInstanceShoudHaveAdminRole(string userName, string userPassword, string userEmail)
        {
            //arrange
            Registered registered = new Registered((userName, userEmail, userPassword));


            var role = UserManager.GetCurrentUserRole();

            //act
            var expectedRole = UserManager.Roles.Admin;

            //assert
            Assert.AreEqual(expectedRole, role, message: "Expected role guest.");
        }
        [TestCase("1", "1", "1")]
        public void RegisterInstanceShouldThoughExceptionIfNoUserWithCredentialsExist(string userName, string userPassword, string userEmail)
        {
            //arrange
            var role = UserManager.GetCurrentUserRole();
            var expectedEx = typeof(Exception);

            //act
            var actEx = Assert.Catch(() => new Registered((userName, userEmail, userPassword)));

            //assert
            Assert.AreEqual(expectedEx, actEx.GetType(), message: "Register constructor works incrolectly");
        }
        [TestCase("oleg", "user", "oleg@mail.com")]
        public void RegisterInstanceShoudHaveRegisteredRole(string userName, string userPassword, string userEmail)
        {
            //arrange
            Registered registered = new Registered((userName, userEmail, userPassword));


            var role = UserManager.GetCurrentUserRole();

            //act
            var expectedRole = UserManager.Roles.Registered;

            //assert
            Assert.AreEqual(expectedRole, role, message: "Expected role guest.");
        }
        [TestCase("1", "1", "1")]
        public void AdminInstanceShouldThoughExceptionIfNoUserWithCredentialsExist(string userName, string userPassword, string userEmail)
        {
            //arrange
            var role = UserManager.GetCurrentUserRole();
            var expectedEx = typeof(Exception);

            //act
            var actEx = Assert.Catch(() => new Admin((userName, userEmail, userPassword)));

            //assert
            Assert.AreEqual(expectedEx, actEx.GetType(), message: "Admin constructor works incrolectly");
        }
        [Test]
        public void ProductManagerGetUserOrderShouldThroughExceptionIfRoleGuest()
        {
            //arrange
            var expectedEx = typeof(Exception);
            //act
            var actEx = Assert.Catch(() => UserManager.GetUserOrders());
            //assert
            Assert.AreEqual(expectedEx, actEx.GetType(), "UserManager.GetUserOrder Works incoreclty.");
        }
        [TestCase("1")]
        [TestCase("test@mail.com")]
        [TestCase("oleg@mail.com")]
        public void ProductManagerGetUserOrderWithOverloadShouldThroughExceptionIfRoleGuest(string email)
        {
            //arrange
            var expectedEx = typeof(Exception);
            //act
            var actEx = Assert.Catch(() => UserManager.GetUserOrders(email));
            //assert
            Assert.AreEqual(expectedEx, actEx.GetType(), "UserManager.GetUserOrder Works incoreclty.");
        }
        //[TestCase("oleg", "user", "oleg@mail.com", "Cucumber")]
        //[TestCase("oleg", "user", "oleg@mail.com", "Mercedes")]
        //public void NAME (string userName, string userPassword, string userEmail, string productName)
        //{
        //    //arrange
        //    Registered registered = new Registered((userName, userEmail, userPassword));
        //    bool expectedResult = true;
        //    //act
        //    bool result = ProductManager.TryEditProduct(productName);
        //    //assert
        //    Assert.AreEqual(expectedResult, result, "TryEditProduct method works incorectly");
        //}
        //[TestCase("oleg", "user", "oleg@mail.com")]
        //public void TryLogoutRegisterUserShouldWorkCorrect(string name, string password, string email)
        //{
        //    //arrange
        //    var registerUser = new Registered((name, email, password));
        //    var expectedEx = typeof(Exception);
        //    //act
        //    registerUser.Logout();
        //    var actEx = Assert.Catch(() => registerUser.EditProfile());
        //    //assert
        //    Assert.AreEqual(expectedEx, actEx.GetType(), "UserManager.GetUserOrder Works incoreclty.");
        //}
    }
}