﻿using System;
using TEST.Entities;
using TEST.Interfaces;
using TEST.RoledUsers;

namespace TEST
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int exitCode = -1;
            IGuest item;

            do
            {
                item = UserManager.GetCurrentUser();
                exitCode = item.ShowCommands();
                
            } while (exitCode != 0);
        }
    }
}
