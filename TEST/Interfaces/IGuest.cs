﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST.Interfaces
{
    public interface IGuest
    {
        
        void ShowProducts();
        void SearchProduct();
        int ShowCommands();
    }
}
