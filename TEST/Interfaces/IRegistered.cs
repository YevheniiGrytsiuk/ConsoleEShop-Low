﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST.Interfaces
{
    public interface IRegistered
    {
        public void CreateNewOrder();
        public void FormOrder();
        public void ShowOrderHistory();
        public void EditProfile();
        public void Logout();
    }
}
