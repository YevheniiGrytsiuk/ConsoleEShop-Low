﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST.Entities
{
    public class User
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }

        public User()
        {
            Orders = new List<Order>();
        }
        public List<Order> Orders { get; set; }
    }
}
