﻿using System;
using System.Collections.Generic;
using System.Text;
using TEST.Entities;
using TEST.Interfaces;

namespace TEST.RoledUsers
{
    public class Registered : Guest, IRegistered
    {
        protected override int Role => (int)UserManager.Roles.Registered;
        protected virtual string Name { get; }
        protected virtual string Email { get; }
        protected virtual string Password { get; }

        public Registered((string name, string email, string password)userInfo)
        {
            if (!Login(userInfo.email, userInfo.password))
                throw new Exception();
        
            Name = userInfo.name;
            Email = userInfo.email;
            Password = userInfo.password;
        }
        public override int ShowCommands()
        {
            int key = 10;

            do
            {
                Console.WriteLine($"Current status: {this.Role}");
                Console.WriteLine($"User name: {this.Name}");
                Console.WriteLine("1. Show products");
                Console.WriteLine("2. Search product by name");
                Console.WriteLine("3. Create new order");
                Console.WriteLine("4. Form order or cancel");
                Console.WriteLine("5. Show order history");
                Console.WriteLine("6. Edit profile");

                Console.WriteLine("9. Logout");
                Console.WriteLine("0. Exit");

                key = Convert.ToInt32(Console.ReadLine());

                Console.Clear();

                switch (key)
                {
                    case 1:
                        {
                            ShowProducts();
                        }
                        break;
                    case 2:
                        {
                            SearchProduct();
                        }
                        break;
                    case 3:
                        {
                            CreateNewOrder();
                        }
                        break;
                    case 4:
                        {
                            FormOrder();
                        }
                        break;
                    case 5: 
                        {
                            ShowOrderHistory();
                        }break;
                    case 6: 
                        {
                            Console.Write("Write new name: ");
                            string name = Console.ReadLine();

                            Console.Write("Write new email: ");
                            string email = Console.ReadLine();

                            Console.Write("Write new password: ");
                            string password = Console.ReadLine();

                            UserManager.TryEditProfile(this.Email, name, email, password);  
                        } break;

                    case 9:
                        { 
                            if (UserManager.TryLogout()) 
                            {
                                return 9;
                            }
                        }
                        break;
                    case 0:
                        {
                            key = 0;
                        }
                        break;
                    default:
                        break;
                }

                Console.ReadKey();
                Console.Clear();

            } while (key > 1);

            Console.ReadKey();
            Console.Clear();
            return key;
        }

        public override void ShowProducts()
        {
            base.ShowProducts();
        }

        public override void SearchProduct()
        {
            base.SearchProduct();
        }

        public virtual void CreateNewOrder()
        {
            ProductManager.ShowProducts();

            Console.WriteLine("Select products by name (enter to continue)");

            string key = "1";
            string[] productsName = new string[1];

            for (int i = 0; !string.IsNullOrWhiteSpace(key); i++)
            {
                Console.Write("->");
                key = Console.ReadLine();
                
                if (string.IsNullOrWhiteSpace(key))
                    break;

                productsName[i] = key;
                Array.Resize(ref productsName, productsName.Length + 1);
            }

            ProductManager.TryCreateNewOrder(this.Email, productsName);
        }

        public virtual void FormOrder()
        {

            if (!ProductManager.ShowUserOrderHistory()) 
            { 
                Console.WriteLine("There is no order history");
                return;
            }

            string key;
            string status;

            do
            {
                Console.Write("Select index -> ");
                key = Console.ReadLine();
                Console.Write("Select status -> ");
                status = Console.ReadLine();

                if (String.IsNullOrWhiteSpace(key) || String.IsNullOrWhiteSpace(status))
                    break;
                ProductManager.TryEditOrder(key,  status);

            } while (true);
        }

        public virtual void ShowOrderHistory()
        {
            ProductManager.ShowUserOrderHistory(showNew: false);
        }

        public virtual void EditProfile()
        {

            Console.Write("Enter new name: ");
            string name = Console.ReadLine();
            Console.Write("Enter new email: ");
            string email = Console.ReadLine();
            Console.Write("Enter new password: ");
            string password = Console.ReadLine();

            UserManager.TryEditProfile(this.Email, name, email, password);
        }

        public virtual void Logout()
        {
            if(UserManager.TryLogout())
                Console.WriteLine("Logout successfully");
            else
                Console.WriteLine("Logut error");
        }
    }
}
