﻿using System;
using System.Collections.Generic;
using System.Text;
using TEST.Interfaces;

namespace TEST.RoledUsers
{
    public class Admin : Registered, IAdmin
    {
        protected override int Role => (int)UserManager.Roles.Admin;

        public Admin((string name, string email, string password) userInfo) : base(userInfo) { }
       
        public override void CreateNewOrder()
        {
            base.CreateNewOrder();
        }

        public override void EditProfile()
        {
            base.EditProfile();
        }

        public override void FormOrder()
        {
            base.FormOrder();
        }

        public override void Logout()
        {
            base.Logout();
        }

        public override void SearchProduct()
        {
            base.SearchProduct();
        }

        public override void ShowOrderHistory()
        {
            base.ShowOrderHistory();
        }

        public override void ShowProducts()
        {
            base.ShowProducts();

            Console.ReadKey();
        }

        public virtual void EditUsers()
        {
            throw new NotImplementedException();
        }
        public override int ShowCommands()
        {
            int key = 0;

            do
            {
                Console.Clear();

                Console.WriteLine($"Current status: {Enum.GetName(typeof(UserManager.Roles), this.Role)}");
                Console.WriteLine($"User name: {this.Name}");
                Console.WriteLine("1. Show products");
                Console.WriteLine("2. Search product by name");
                Console.WriteLine("3. Create new order");
                Console.WriteLine("4. Edit user");
                Console.WriteLine("5. Add new product");
                Console.WriteLine("6. Edit product");
                Console.WriteLine("7. Show all users");
                Console.WriteLine("9. Logout");
                Console.WriteLine("0. Exit");

                key = Convert.ToInt32(Console.ReadLine());

                Console.Clear();

                switch (key)
                {
                    case 1:
                        {
                             ShowProducts();
                        }
                        break;
                    case 2:
                        {
                            SearchProduct();
                        }
                        break;
                    case 3:
                        {
                            CreateNewOrder();
                        }
                        break;
                    case 4:
                        {

                            UserManager.ShowUsers();

                            Console.Write("Enter email: ");
                            string uEmail = Console.ReadLine();

                            var user = UserManager.GetUser(uEmail);

                            if (user != null)
                            {
                                Console.Write("Enter new name: ");
                                string name = Console.ReadLine();
                                Console.Write("Enter new email: ");
                                string email = Console.ReadLine();
                                Console.Write("Enter new password: ");
                                string password = Console.ReadLine();
                                Console.Write("Enter new role: ");
                                string role = Console.ReadLine();

                                
                                if (UserManager.TryEditUser((name, email, password, role), uEmail))
                                    Console.WriteLine("Success");
                            }
                            
                        }
                        break;
                    case 5:
                        {
                            Console.WriteLine("Create new product\n");

                            Console.Write("Product name: ");
                            string name = Console.ReadLine();

                            Console.Write("Product category: ");
                            string category = Console.ReadLine();

                            Console.Write("Product description: ");
                            string description = Console.ReadLine();

                            Console.Write("Product price: ");
                            decimal price = Convert.ToDecimal(Console.ReadLine());

                            ProductManager.TryAddNewProduct(new Entities.Product() { Name = name, Category = category, Description = description, Price = price });
                        }
                        break;
                    case 6:
                        {
                            ProductManager.ShowProducts();

                            Console.Write("Enter name of product to edit: ");
                            string name = Console.ReadLine();

                            ProductManager.TryEditProduct(name);
                        }
                        break;
                    case 7: 
                        {
                            UserManager.ShowUsers();
                            Console.ReadKey();
                        } break;
                    case 9:
                        {
                            Logout();
                            return 9;
                        }
                    case 0:
                        {
                            return 0;
                        }
                    default:
                        break;
                }
            } while (true); 

        }
    }
}
