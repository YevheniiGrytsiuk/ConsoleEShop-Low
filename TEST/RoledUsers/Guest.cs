﻿using System;
using System.Collections.Generic;
using System.Linq;
using TEST.Entities;
using TEST.Interfaces;

namespace TEST.RoledUsers
{
    public class Guest : IGuest
    {
        protected virtual int Role => (int)UserManager.Roles.Guest;
        public virtual int ShowCommands()
        {
            int key = 10;

            do
            {
                Console.WriteLine($"Current status: {Enum.GetName(typeof(UserManager.Roles), this.Role)}");
                Console.WriteLine("1. Show products");
                Console.WriteLine("2. Search product by name");
                Console.WriteLine("3. Register new user");
                Console.WriteLine("4. Login");
                Console.WriteLine("0. Exit");

                key = Convert.ToInt32(Console.ReadLine());

                Console.Clear();

                switch (key)
                {
                    case 1:
                        {
                            ShowProducts();
                        } break;
                    case 2:
                        {
                            SearchProduct();
                        } break;
                    case 3:
                        {
                            Register();
                        } break;
                    case 4:
                        {
                            Console.Write("Enter email: ");
                            string email = Console.ReadLine();

                            Console.Write("Enter password: ");
                            string password = Console.ReadLine();

                            Login(email, password);

                            Console.Clear();

                            return 4;
                        } 
                    case 0:
                        {
                            return 0;
                        }
                    default:
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            } while (true);
        }

        public virtual void SearchProduct()
        {
            Console.Write("Enter name to find product: ");
            string name = Console.ReadLine();

            ProductManager.ShowProduct(name);

            
        }


        public virtual void ShowProducts()
        {
            ProductManager.ShowProducts();
        }

        public virtual void Register()
        {
            Console.Write("Enter name of user: ");
            string name = Console.ReadLine();
            Console.Write("Enter email of user: ");
            string email = Console.ReadLine();
            Console.Write("Enter password of user: ");
            string password = Console.ReadLine();

            if(UserManager.TryRegister(name, email, password))
                Console.WriteLine("Success");
            else
                Console.WriteLine("Something went wrong");
        }
        protected bool Login(string email, string password) 
        {
            return UserManager.TryLogin(email, password);
        }
        
    }
}
