﻿using System;
using System.Collections.Generic;
using System.Linq;
using TEST.Entities;
using TEST.Interfaces;
using TEST.RoledUsers;

namespace TEST
{
    public sealed class UserManager
    {
        private static User currentUser;

        private static List<User> users;

        private static Roles CurrentRole 
        {
            get
            {
                if (currentUser?.Role != null)
                    return (Roles)currentUser.Role;
                return Roles.Guest;
            } 
        }

        public enum Roles
        {
            Guest,
            Registered,
            Admin
        }
        static UserManager()
        {
            users = new List<User>()
            {
                new User()
                {
                    Name = "testName",
                    Email = "test@mail.com",
                    Password = "pwd",
                    Role = (int)Roles.Admin,

                    Orders = new List<Order>()
                    {
                        new Order()
                        {
                            products = new List<Product>()
                            {
                            new Product()
                            {
                                Name = "Banana",
                                Category = "Fruit",
                                Description = "Tasty",
                                Price = 4.5m
                            },
                            new Product()
                            {
                                Name = "Garlic",
                                Category = "Food supplement",
                                Description = "Fresh",
                                Price = 3
                            }
                        },
                        Status = "Pending"
                        },
                        new Order()
                        {
                            products = new List<Product>()
                            {
                            new Product()
                            {
                                Name = "Computer",
                                Category = "Electronics",
                                Description = "Fast",
                                Price = 105.2m
                            },
                            new Product()
                            {
                                Name = "Garlic",
                                Category = "Food supplement",
                                Description = "Fresh",
                                Price = 3
                            }
                        },
                        Status = "New"
                        }
                    }
                },
                new User()
                {
                    Name = "oleg",
                    Email = "oleg@mail.com",
                    Password = "user",
                    Role = (int)Roles.Registered,
                    Orders = new List<Order>()
                    {
                        new Order()
                        {
                            products = new List<Product>()
                            {
                            new Product()
                            {
                                Name = "Cucmber",
                                Category = "Green",
                                Description = "Tasty",
                                Price = 4.5m
                            },
                            new Product()
                            {
                                Name = "Onion",
                                Category = "Food supplement",
                                Description = "Fresh",
                                Price = 3
                            }
                        },
                        Status = "Pending"
                        },
                        new Order()
                        {
                            products = new List<Product>()
                            {
                            new Product()
                            {
                                Name = "Computer",
                                Category = "Electronics",
                                Description = "Fast",
                                Price = 105.2m
                            },
                            new Product()
                            {
                                Name = "Garlic",
                                Category = "Food supplement",
                                Description = "Fresh",
                                Price = 3
                            }
                        },
                        Status = "Pending"
                        }
                    }
                },
                new User()
                {
                   Name = "viktor",
                    Email = "viktor@mail.com",
                    Password = "yolo",
                    Role = (int)Roles.Admin,
                    Orders = new List<Order>()
                    {
                        new Order()
                        {
                            products = new List<Product>()
                            {
                            new Product()
                            {
                                Name = "Water",
                                Category = "Drink",
                                Description = "White",
                                Price = 2.5m
                            },
                            new Product()
                            {
                                Name = "Shovel",
                                Category = "Mocna",
                                Description = "Stainless",
                                Price = 50m
                            },
                            new Product()
                            {
                                Name = "MacBook",
                                Category = "Electronics",
                                Description = "Fast",
                                Price = 1205.2m
                            },
                            new Product()
                            {
                                Name = "Candy",
                                Category = "Food supplement",
                                Description = "Tasty",
                                Price = 12.5m
                            }
                        },
                        Status = "Done"
                        }
                    }
                }
            };
        }
        private UserManager() { }
        //TODO redo
        public static List<Order> GetUserOrders() 
        {
            if(CurrentRole != Roles.Guest)
                return GetUserOrders(currentUser.Email);

            throw new Exception();
        }
        public static List<Order> GetUserOrders(string email)
        {
            if (CurrentRole == Roles.Admin)
                return users.Find(u => u.Email == email).Orders;

            throw new Exception();
        }
        public static Roles GetCurrentUserRole() 
        {
            return CurrentRole;
        }
        public static bool TryLogin(string email, string password) 
        {
            //if (CurrentRole != Roles.Guest)
            //    return false;

            var user = users.Find(usr => usr.Email == email && usr.Password == password);
            
            if (user != null)
            {
                currentUser = user;
                return true;
            }
            return false;
        }
        public static bool TryRegister(string name, string email, string password)
        {
            if (!(CurrentRole == Roles.Guest))
                return false;
            if (users.Where(u => u.Email == email).Any())
                return false;

            users.Add
                (
                    new User()
                    {
                        Name = name,
                        Email = email,
                        Password = password,
                        Role = (int)Roles.Registered
                    }
                );
            return true;
        }
        public static void ShowUsers() 
        {
            if (CurrentRole == Roles.Admin)
            {
                foreach (var user in users)
                {
                    Console.WriteLine($"User name -> {user.Name}");
                    Console.WriteLine($"User email -> {user.Email}");
                    Console.WriteLine($"User password -> {user.Password}");
                    Console.WriteLine($"User role -> {Enum.GetName(typeof(Roles), user.Role)}");
                    Console.WriteLine($"Has -> {user.Orders.Count} number of orders");
                    Console.WriteLine();
                }
            }
        }
        public static bool TryLogout() 
        {
            if (CurrentRole == Roles.Registered || CurrentRole == Roles.Admin)
            { 
                currentUser = null;
                return true;
            }
            return false;
        }
        public static IGuest GetCurrentUser() 
        {
            if (CurrentRole == Roles.Guest)
                return new Guest();
            else if (CurrentRole == Roles.Registered)
                return new Registered((currentUser.Name, currentUser.Email, currentUser.Password));
            else
                return new Admin((currentUser.Name, currentUser.Email, currentUser.Password));
        }
        public static bool TryEditProfile(string oldEmail, string newName, string newEmail, string newPassword)
        {
            if(CurrentRole == Roles.Registered || CurrentRole == Roles.Admin)
            {
                var dasUser = users.Where(u => u.Email == oldEmail).First();

                if (dasUser != null) 
                {
                    dasUser.Name = newName;
                    dasUser.Email = newEmail;
                    dasUser.Password = newPassword;
                    //TryLogin(dasUser.Email, dasUser.Password);
                }

                return true;
            }
            throw new Exception();
        }
        public static bool TryWatchUsers()
        {
            if (CurrentRole != Roles.Admin)
                return false;

            foreach (var user in users)
            {
                Console.WriteLine($"User name: {user.Name}");
                Console.WriteLine($"User email: {user.Email}");
                Console.WriteLine($"User password: {user.Password}");
                Console.WriteLine($"User role: {Enum.GetName(typeof(Roles), user.Role)}");
                ProductManager.ShowUserOrderHistory(user.Email);
                Console.WriteLine();
            }
            return true;
        }
        public static bool TryEditUser((string name, string email, string password, string role) userInfo, string uEmail)
        {
            var user = users.Find(u => u.Email == uEmail);
            
            if (user != null) 
            {
                user.Name = userInfo.name;
                user.Email = userInfo.email;
                user.Password = userInfo.password;
                user.Role = Convert.ToInt32(userInfo.role);

                ProductManager.ShowUserOrderHistory(user.Email);

                while (true) 
                {
                    Console.WriteLine("Enter index:");
                    string index = Console.ReadLine();
                    Console.WriteLine("Enter status: ");
                    string status = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(index) || string.IsNullOrWhiteSpace(status))
                        break;
                    ProductManager.TryEditOrder(index, status, true);
                }
                return true;
            }
            
            return false;
        }

        public static User GetUser(string email) 
        {
            if (CurrentRole != Roles.Admin)
                return null;

            return users.Find(u => u.Email == email);
        }
    }
    
}
