﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TEST.Entities;

namespace TEST
{
    public class ProductManager
    {
        private static List<Product> products;
        static ProductManager()
        {
            products = new List<Product>()
            {
                new Product()
                {
                    Name = "Cucumber",
                    Category = "Vegetable",
                    Description = "Green and tasty",
                    Price = 3.2m
                },
                new Product()
                {
                    Name = "Watermelon",
                    Category = "Fruit",
                    Description = "Sweet and heavy",
                    Price = 5m
                },
                new Product()
                {
                    Name = "Mercedes",
                    Category = "Car",
                    Description = "Fast car",
                    Price = 2500m
                }
            };
        }
        public static bool TryAddNewProduct(Product product)
        {
            products.Add(product);

            return true;
        }
        public static void ShowProducts()
        {
            int index = 0;

            foreach (var product in products)
            {
                Console.WriteLine($"#{index} {product.Name}");
                Console.WriteLine($"#{index} Description -> {product.Description}");
                Console.WriteLine($"#{index} Category -> {product.Category}");
                Console.WriteLine($"#{index} Price -> {product.Price}");
                Console.WriteLine();
                index++;
            }
        }
        public static void ShowProduct(string name)
        {
            var product = products.Find(p => p.Name == name);

            if (product != null)
            {
                Console.WriteLine($"{product.Name}");
                Console.WriteLine($"Description {product.Description}");
                Console.WriteLine($"Category {product.Category}");
                Console.WriteLine($"Price {product.Price}");
            }
            else
                Console.WriteLine("No product fount");
        }
        public static bool TryEditProduct(string productName)
        {
            if (UserManager.GetCurrentUserRole() != UserManager.Roles.Admin)
                return false;

            var product = products.Find(p => p.Name == productName);

            Console.WriteLine("Edit product\n");

            Console.Write($"Enter new ({product.Name}) name: ");
            product.Name = Console.ReadLine();

            Console.Write($"Enter new ({product.Description}) description: ");
            product.Description = Console.ReadLine();

            Console.Write($"Enter new ({product.Category}) category: ");
            product.Category = Console.ReadLine();

            Console.Write($"Enter new ({product.Price}) price: ");
            product.Price = Convert.ToDecimal(Console.ReadLine());

            return true;
        }

        public static bool TryCreateNewOrder(string userEmail, params string[] names)
        {
            if (UserManager.GetCurrentUserRole() != UserManager.Roles.Guest)
            {
                var user = UserManager.GetUserOrders(userEmail);


                Order order = new Order();
                order.products = new List<Product>();
                user.Add(order);

                foreach (var name in names)
                {
                    var product = products.Find(p => p.Name == name);

                    if (product != null)
                    {
                        order.products.Add(product);
                    }
                }
                order.Status = "New";
                return true;
            }
            return false;
        }

        public static void TryEditOrder(string index, string status)
        {
            int i = Convert.ToInt32(index);
            UserManager.GetUserOrders()
                .Where(o => o.Status == "New" || o.Status == "Canceled" || o.Status == "Got")
                .ElementAt(i).Status = status;
        }
        public static void TryEditOrder(string index, string status, bool ignoreNew)
        {
            int i = Convert.ToInt32(index);
            var u = UserManager.GetUserOrders()
                .ElementAt(i).Status = status;
        }

        //public static void ShowUserOrderHistory()
        //{
        //    var userOrder = UserManager.GetUserOrders("1");

        //    foreach (var order in userOrder)
        //    {
        //        foreach (var product in order.products)
        //        {
        //            Console.WriteLine($"Name {product.Name}");
        //            Console.WriteLine($"Description {product.Description}");
        //            Console.WriteLine($"Category {product.Category}");
        //            Console.WriteLine($"Price {product.Price}");
        //        }
        //        Console.WriteLine($"Status {order.Status}");
        //        Console.WriteLine();
        //    }
        //}

        public static void ShowUserOrderHistory(string email)
        {
            var userOrders = UserManager.GetUserOrders(email);

            foreach (var order in userOrders)
            {
                foreach (var product in order.products)
                {
                    Console.WriteLine($"{product.Name}");
                    Console.WriteLine($"{product.Description}");
                    Console.WriteLine($"{product.Category}");
                    Console.WriteLine($"{product.Price}");
                }
                Console.WriteLine($"{order.Status}");
            }
        }
        public static bool ShowUserOrderHistory(bool showNew = false)
        {
            var userOrder = UserManager.GetUserOrders();

            if (userOrder.Count == 0)
                return false;

            if (showNew)
                userOrder = userOrder.Where(o => o.Status == "New" || o.Status == "Canceled" || o.Status == "Got").ToList();

            foreach (var order in userOrder)
            {
                int index = 0;

                foreach (var product in order.products)
                {
                    Console.WriteLine($"#{index} Product name: {product.Name}");
                    Console.WriteLine($"#{index} Product description: {product.Description}");
                    Console.WriteLine($"#{index} Product category: {product.Category}");
                    Console.WriteLine($"#{index} Product price: {product.Price}");
                }
                Console.WriteLine($"#{index} Order status: {order.Status}");
                index++;

                Console.WriteLine();
            }
            return true;
        }
    }
}
